import React from 'react';
import {NavLink} from "react-router-dom"
// import p from './Planets1.module.css';

const Planets1Items = (props) => {
  let path1 = "/objects/planets/" + props.id;
  return <div>
    <NavLink to={path1}>{props.name}</NavLink>
  </div>
}

const Planets1 = (props) => {
  let Planets1Template = props.Planets1Base
  .map( Template => <Planets1Items name={Template.name} id={Template.id}/> );
  return <div> {Planets1Template} </div>
}

export default Planets1;
