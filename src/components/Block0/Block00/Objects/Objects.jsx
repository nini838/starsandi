import React from 'react';
import ob from './Objects.module.css';
import Block1 from '../Block000/Block1/Block1';
import Block2 from '../Block000/Block2/Block2';
import Wall1 from '../Block000/Wall1/Wall1';

const Objects = (props) => {
  return (
    <div className={ob.Objects}>
      <Block1/>
      <Block2 state={props.state}/>
      <Wall1/>
    </div>
  )
}

export default Objects;
