import React from 'react';
import {NavLink} from "react-router-dom"
// import n from './NaturalBody1.module.css';

const NaturalBody1Items = (props) => {
  let path1 = "/objects/naturalBody/" + props.id;
  return <div>
    <NavLink to={path1}>{props.name}</NavLink>
  </div>
}

const NaturalBody1 = (props) => {
  let NaturalBody1Template = props.NaturalBody1Base
  .map( Template => <NaturalBody1Items name={Template.name} id={Template.id}/> );
  return <div> {NaturalBody1Template} </div>
}

export default NaturalBody1;
