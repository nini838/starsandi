import React from 'react';
import b from './Block1.module.css';
import {NavLink} from "react-router-dom"

const Block1 = () => {
  return (
    <div className={b.Block1}>
      <div><NavLink to="/objects/stars">Звезды</NavLink></div>
      <div><NavLink to="/objects/planets">Планеты</NavLink></div>
      <div><NavLink to="/objects/naturalBody">Малые небесные тела</NavLink></div>
    </div>
  )
}

export default Block1;
