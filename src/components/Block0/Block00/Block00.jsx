import React from 'react';
import b0 from './Block00.module.css';
import Home from './Home/Home';
import Objects from './Objects/Objects';
import Hobby from './Hobby/Hobby';
import ChatRoom from './ChatRoom/ChatRoom';
import {Route} from "react-router-dom"

const Block00 = (props) => {
  return (
    <div className={b0.Block00}>
      <Route path='/home' render={ () => <Home/> }/>
      <Route path='/objects' render={ () => <Objects state={props.state}/> }/>
      <Route path='/hobby' render={ () => <Hobby/> }/>
      <Route path='/chatRoom' render={ () => <ChatRoom/> }/>
    </div>
  )
}

export default Block00;
