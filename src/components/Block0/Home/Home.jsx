import React from 'react';
import b from './Home.module.css';

const Home = () => {
  return (
    <div className={b.Home}>
      <h1>Do you believe in human beings?</h1>
      <h1>Do you resign as a human being?</h1>
    </div>
  )
}

export default Home;
