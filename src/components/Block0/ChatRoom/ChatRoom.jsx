import React from 'react';
import b from './ChatRoom.module.css';
import ChatInfo from './ChatInfo/ChatInfo';
import InputWindow from './InputWindow/InputWindow';
import Posts from './Posts/Posts';

const ChatRoom = () => {
  return (
    <div className={b.ChatRoom}>
      <ChatInfo/>
      <InputWindow/>
      <Posts/>
    </div>
  )
}

export default ChatRoom;
