import React from 'react';
import b0 from './Block0.module.css';
import Home from './Home/Home';
import Objects from './Block00/Objects/Objects';
import Hobby from './Block00/Hobby/Hobby';
import ChatRoom from './ChatRoom/ChatRoom';
import {Route} from "react-router-dom"

const Block0 = (props) => {
  return (
    <div className={b0.Block0}>
      <Route path='/home' render={ () => <Home/> }/>
      <Route path='/objects' render={ () => <Objects state={props.state}/> }/>
      <Route path='/hobby' render={ () => <Hobby/> }/>
      <Route path='/chatRoom' render={ () => <ChatRoom/> }/>
    </div>
  )
}

export default Block0;
