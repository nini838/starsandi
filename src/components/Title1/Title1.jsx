import React from 'react';
import t from './Title1.module.css';

const Title1 = () => {
  return <header className={t.Title1}>
    <div>
      Детка, ты просто космос...
    </div>
  </header>
}

export default Title1;
