import './App.css';
import {BrowserRouter} from "react-router-dom"
import Title1 from './components/Title1/Title1';
import Menu1 from './components/Menu1/Menu1';
import Block0 from './components/Block0/Block0';
import Comments1 from './components/Comments1/Comments1';

const App = (props) => {
  return (
    <BrowserRouter>
      <div className="JoJo">
        <Title1/>
        <Menu1/>
        <Block0 state={props.state.stateBlock2} />
        <Comments1/>
      </div>
    </BrowserRouter>
  );
}

export default App;
